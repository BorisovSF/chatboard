'use strict';

module.exports = function() {
  $.gulp.task('clean', function(cb) {
    return $.del($.config.root, cb);
  });

    $.gulp.task('clean:server', function(cb) {
        return $.del($.config.server, cb);
    });
};

