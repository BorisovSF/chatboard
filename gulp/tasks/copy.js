/**
 * Created by User on 11.02.2017.
 */
'use strict';

module.exports = function() {

    $.gulp.task('copy:fonts', function() {
        return $.gulp.src('./source/fonts/**/*.*', { since: $.gulp.lastRun('copy:fonts') })
            .pipe($.gulp.dest($.config.root + '/assets/fonts'));
    });
    $.gulp.task('copy:image', function() {
        return $.gulp.src('./source/images/**/*.*', { since: $.gulp.lastRun('copy:image') })
            .pipe($.gulp.dest($.config.root + '/assets/img'));
    });
  $.gulp.task('copy:favicon', function() {
    return $.gulp.src('./source/favicon.ico', { since: $.gulp.lastRun('copy:favicon') })
      .pipe($.gulp.dest($.config.root));
  });
    $.gulp.task('copy:image-release', function() {
        return $.gulp.src('./source/images/**/*.*', { since: $.gulp.lastRun('copy:image') })
            .pipe($.gp.tinypng('Qt0rYOPqMmad983gXtwnOEhNrMCaFELA'))
            .pipe($.gulp.dest($.config.root + '/assets/img'));
    });
    $.gulp.task('copy:server', function() {
        return $.gulp.src($.config.root + '/**/*.*').pipe($.gulp.dest($.config.server));
    });
    $.gulp.task('copy',
        $.gulp.parallel(
            'copy:fonts',
            'copy:image'
        ));
    $.gulp.task('copy-release',
        $.gulp.parallel(
            'copy:fonts',
            'copy:image-release'
        ));
};
