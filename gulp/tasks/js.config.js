'use strict';

module.exports = function() {
  $.gulp.task('js:config', function() {
    return $.gulp.src('./source/js/config/*.js')
      .pipe($.gp.sourcemaps.init())
      .pipe($.gp.concat('config.js'))
      .pipe($.gp.sourcemaps.write())
      .pipe($.gulp.dest($.config.root + '/assets/js'))
  })
};
