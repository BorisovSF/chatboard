'use strict';

module.exports = function() {
  $.gulp.task('watch', function() {
    $.gulp.watch('./source/js/libs/*.js', $.gulp.series('js:process'));
    $.gulp.watch('./source/js/modules/*.js', $.gulp.series('js:foundation'));
    $.gulp.watch('./source/js/config/*.js', $.gulp.series('js:config'));
    $.gulp.watch('./source/style/**/*.scss', $.gulp.series('sass'));
    $.gulp.watch('./source/template/**/*.pug', $.gulp.series('pug'));
    $.gulp.watch('./source/template/includes/*.pug', $.gulp.series('pug_includes'));
    $.gulp.watch('./source/images/**/*.*', $.gulp.series('copy:image'));
  });
};
