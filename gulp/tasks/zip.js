'use strict';

module.exports = function() {
  let d = new Date();
  let cday = '';
  let buf = d.getDate();
  if(buf < 10) buf = '0'+buf;
  cday += buf;
  buf = d.getMonth()+1;
  if(buf < 10) buf = '0'+buf;
  cday += buf;
  buf = d.getFullYear().toString().substr(2,2);
  cday += buf;
  $.gulp.task('zip', function() {
    return $.gulp.src(['./build/assets/**/*', './build/index.html', './build/favicon.ico'],{ base: '.' })
      .pipe($.zip('magnit_'+cday+'.zip'))
      .pipe($.gulp.dest('dist'))
  });
};
