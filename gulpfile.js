'use strict';

global.$ = {
  package: require('./package.json'),
  config: require('./gulp/config'),
  path: {
    task: require('./gulp/paths/tasks.js'),
    jsFoundation: require('./gulp/paths/js.foundation.js'),
    cssFoundation: require('./gulp/paths/css.foundation.js'),
    app: require('./gulp/paths/app.js')
  },
  gulp: require('gulp'),
  del: require('del'),
  cssunit: require('gulp-css-unit'),
  minify: require('gulp-minify'),
  cleanCSS: require('gulp-clean-css'),
  browserSync: require('browser-sync').create(),
  gp: require('gulp-load-plugins')(),
  zip: require('gulp-zip'),
  browserify: require('gulp-browserify')
};

$.path.task.forEach(function(taskPath) {
  require(taskPath)();
});

$.gulp.task('default', $.gulp.series(
    'clean',
  $.gulp.parallel(
    'sass',
    'pug',
    'pug_includes',
    'js:foundation',
    'js:process',
    'js:config',
    'copy',
    'css:foundation'),
    $.gulp.parallel(
    'watch',
    'serve'
    )

));

$.gulp.task('release', $.gulp.series(
  'clean',
  $.gulp.parallel(
    'sass',
    'pug',
    'pug_includes',
    'js:foundation',
    'js:process',
    'js:config',
    'copy',
    'css:foundation'),
  'zip'
));
