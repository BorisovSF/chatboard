'use strict';
let getJS_timer;

function getJS() {
    clearTimeout(getJS_timer);
    $.ajax({
        type: 'GET',
        url: api_chat,
        dataType: 'json',
        xhrFields: { withCredentials: true },
        success: (data) => {
            data = data.data;
            for(let mes of data) {
                msg_constructor(mes);
            }
        },
        error: (e) => {
            console.error(e);
        },
        always: () => {
            getJS_timer = setTimeout(() => {getJS()}, 2000);
            console.log(getJS_timer);
        }
    });
}

// Отправка сообщения
function add_question() {
    let m_form = $('.message_form');
    let m_question = $('.area_question');
    let send = $('.send');
    let open_field = $('.icon_add');
    // при нажатии на Enter происходит отправка формы
    m_question.keypress((e) => { if(e.keyCode === 13) send.click(); });
    // убираем возможность переноса строки при нажатии Enter
    m_question.keypress((event) => {
       let keyCode = event.which;
       if (keyCode === 13) {
           event.preventDefault();
       }
    });
    m_form.hide();
    open_field.on('click', () => {
        m_form.slideToggle();
    });
    send.on('click', () => {
        if (m_question.val().length === 0) {
            alert('Введите текст комментария!');
            m_question.focus();
        } else {
            m_form.hide('500');
            m_question.val('');
        }
    });
}

// Блок с вводом комментария
function TextArea() {
    let text_area = $('.area_question');
    let count_symbol = $('.remainder');
    let maxCount = 300;
    count_symbol.html(maxCount);
    text_area.keyup(function() {
        let lenText = this.value.length;

        if (this.value.length >= maxCount) {
            this.value = this.value.substr(0, maxCount);
        }

        let cnt = maxCount - lenText;

        if(cnt <= 0){
            count_symbol.html('0');
        } else {
            count_symbol.html(cnt);
        }
    });
}

/*Конструктор блоков с сообщениями*/
function msg_constructor(data_msg) {
    let panel_wr = $('.panel_wrap');
    if (panel_wr.children('.panel[data-id=' + data_msg.MessageID + ']').length === 0) {
        // Имя пользователя
        let user_name = data_msg.UserName;
        let last_name = data_msg.UserLastName;
        // Время
        let date_time = data_msg.CreateDateTime.slice(11, 19);
        //Конструктор панели
        let panel = $('<div>').addClass('panel');
        let member = $('<div>').addClass('member');

        let querist = $('<span>').addClass('querist').text(last_name + ' ' + user_name);
        let question_time = $('<span>').addClass('question_time').text(date_time);
        member.append(querist).append(question_time);
        let question = $('<div>').addClass('question').append($('<span>').addClass('question_text').text(data_msg.MessageText));
        panel.append(member).append(question);
        panel.attr('data-id', data_msg.MessageID);
        panel_wr.append(panel);
    }
}

// $.ajax({
//     type: 'PUT',
//     url: api_send_msg,
//     dataType: 'json',
//     data: {
//       id: 'user_id'
//     },
//     success: () => {
//
//     },
//     error: (e) => {
//         console.error(e);
//     }
// });

$(document).ready(() => {
    getJS();
    add_question();
    TextArea();
});