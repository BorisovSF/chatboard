'use strict';
let chat;
class Chat {
    constructor() {
        console.log('Chat constructor');
        $('#home').click(() => { chat.home();} );
        $('#logout').click(() => { chat.logout();} );
    }
    home() {
        document.location.href = 'http://softingmsk.ru/';
    };
    logout() {
        $.ajax({
            url: loc_server_api+'auth',
            method: 'DELETE',
            cache: false,
            crossDomain: true,
            dataType: 'json',
            xhrFields: { withCredentials: true },
            complete: function() {
                document.location.href = 'index.html';
            }
        });
    }
}