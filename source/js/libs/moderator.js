function menu_btn() {
    let burger = $('.burger_btn');
    let menu_list = $('.buttons_panel');

    burger.on('click', function () {
        menu_list.slideToggle(300, function(){
            if( $(this).css('display') === "none"){
                $(this).removeAttr('style');
            }
        });
    })
}

$(document).ready(function () {
    menu_btn()
});