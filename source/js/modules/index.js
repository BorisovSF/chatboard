'use strict';
let index;

class Index {
    constructor() {
        console.log('index constructor');
        let auth_btn = $('.entry');
        let log_name = $('.login_name');
        let username = log_name.val();
        let err = $('.err_entry');

        auth_btn.click(() => {index.auth(username, log_name, err);});
        log_name.focus().keypress((k) => {
            if (k.keyCode === 13) auth_btn.click();
        });
    }

    auth(username, log_name, err) {
        $.when(this.auth_ajax(username)).done((data) => {
            console.log(data);
            err.hide(data);
            log_name.css('border-bottom', '3px solid #00BC8C');
        }).fail((e) => {
            console.error(e);
            err.show(e);
            log_name.focus().css('border-bottom', '3px solid #E74C3C');
        });
    }

    auth_ajax(username) {
        let def = $.Deferred();

        $.ajax({
            type: 'GET',
            url: api_reg,
            dataType: 'json',
            data: {
                UserName: username,
                LastName: 'LN',
                Position: '3',
                Organization: '4',
                Email: '5',
                Telephone: '6',
                Address: '7'
            },
            xhrFields: { withCredentials: true },
            success: (json, status, xhr) => {
                if (username.trim() !== '' && json.success === true && xhr.status === 200) {
                    def.resolve({data:json});
                    document.location.href = 'chat.html';
                } else {
                    def.reject({data:json});
                }
            },
            error: function(e) {
                def.reject(e);
            }
        });

        return def;
    }
}